<!-- `quesadilla` - an elegant background task queue for the more civilized age
Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

All development for this project happens on [GitLab](https://gitlab.com/arcanery/python/quesadilla/connectors/postgresql-sqlalchemy).

## Preparing a development environment

### Clone the project and submodules

In your workspace directory:

```bash
git clone git@gitlab.com:arcanery/python/quesadilla/connectors/postgresql-sqlalchemy.git
cd quesadilla/
pre-commit install
```

### Virtual environment

Create a new virtual environment and activate it:

```bash
python3.12 -m venv .venv
source .venv/bin/activate
python -m pip install --upgrade pip
```

If you have a preferred way of managing your virtual environments, feel free to substitute it.

### Install `poetry`

```bash
python -m pip install poetry
```

### Install the package locally

```bash
python -m poetry install --sync --all-extras
```

## Testing

```bash
python -m pytest -v tests/
```

### Test reporting

Run tests with a JUnit report and generate a HTML coverage report, then serve it on
[http://localhost:19000/](http://localhost:19000/):

```bash
python -m coverage run -m pytest --junitxml=junit.xml --verbosity=1 tests/ && python -m coverage html
python -m http.server -d htmlcov/ 19000
```

### Skipping slow tests

```bash
python -m pytest -v -m "not slow" tests/
```

## Documentation

Run the documentation development server:

```bash
python -m mkdocs serve --dev-addr 127.0.0.1:18000
```
